
## Description

This project has been setup for anyone who wants a good base node js app to start with

## Installation
#To start up the docker containers, run this command in the project root:
```
$ yarn docker:bash

```

## Generate your Thumbnail
#To start you need to sign up with

```
http://localhost:3005/api/user/register

{
    "username": "toyirrn",
    "password": "12345678"
}
```

#create your thumbnail with. (put and image url you want and put your file name)
```
Autorization Bearer token...

http://localhost:3005/api/thumbnail/create

{
    "image_url": "https://lh3.googleusercontent.com/ALrXQhTm_DAJCkShULK6_a4HdDOk_WiIXn1-Vkidm5ET2bcv-duVn9XfSHPXCTEnFxRypK7qKp-ifgaOCE3vdhUk",
    "file_name": "testimg"
}
```

#JSON patching (specify your object and your patch)
```
Autorization Bearer token...

http://localhost:3005/api/patch/create

{
  "obj": {
  "baz": "qux",
  "foo": "bar"
  },
  "patch": [
    { "op": "replace", "path": "/baz", "value": "boo" },
    { "op": "add", "path": "/hello", "value": ["world"] },
    { "op": "remove", "path": "/foo" }
  ]
}
```

## Instructions
Remember to create your own .env file with your variables. You can check .env.example
Also JSON patch and thumbnail routes are secured. Remember to put the token you get when you register in the Autorization header 

## Stay in touch with me
- Author - [Erin Deji](erin.deji@gmail.com)
- Instagram - [https://www.instagram.com/ande_lifa/](https://www.instagram.com/ande_lifa/)
- Twitter - [@ande_oggz](https://twitter.com/ande_oggz)

## License

  This project is [MIT licensed](LICENSE).
