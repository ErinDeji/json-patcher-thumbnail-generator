"use strict";

require("dotenv").config();
const PORT = process.env.PORT;
const ENV = process.env.ENV;
const JWT_SECRET = process.env.JWT_SECRET;
const BASE_URL=  process.env.BASE_URL;

let MONGODB_URI = process.env.DATABASE;

if (ENV === "development") {
  MONGODB_URI = "mongodb://database";
}

module.exports = {
  ENV,
  PORT,
  MONGODB_URI,
  JWT_SECRET,
  BASE_URL,
  cloudinary: {
		cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
		api_key: process.env.CLOUDINARY_API_KEY,
		api_secret: process.env.CLOUDINARY_API_SECRET,
	},
};
