"use strict";
const fs = require("fs");
const serviceLocator = require("./service-locator");
const turboLogger = require("turbo-logger").createStream({});
const jsonPatch = require("json-patch");
const Axios = require("axios");

const UserService = require("../services/user");
const UserController = require("../controllers/user");
const PatchService = require("../services/json-patch");
const PatchController = require("../controllers/json-patch");
const ThumbnailService = require("../services/thumbnail");
const ThumbnailController = require("../controllers/thumbnail");
const Uploader = require("../utils/uploader");

serviceLocator.register("logger", () => {
  return turboLogger;
});

serviceLocator.register("axios", () => {
  return Axios;
});

serviceLocator.register("json-patch", () => {
  return jsonPatch;
});

serviceLocator.register("fs", () => {
  return fs;
});

serviceLocator.register("cloudinary", () => {
  const logger = serviceLocator.get("logger");
  return new Uploader(logger);
});

serviceLocator.register("userService", () => {
  const logger = serviceLocator.get("logger");

  return new UserService(logger);
});

serviceLocator.register("userController", () => {
  const userService = serviceLocator.get("userService");
  const logger = serviceLocator.get("logger");

  return new UserController(userService, logger);
});

serviceLocator.register("patchService", () => {
  const logger = serviceLocator.get("logger");
  const fastJsonPatch = serviceLocator.get("json-patch");

  return new PatchService(logger, fastJsonPatch);
});

serviceLocator.register("patchController", () => {
  const patchService = serviceLocator.get("patchService");
  const logger = serviceLocator.get("logger");

  return new PatchController(patchService, logger);
});

serviceLocator.register("thumbnailService", () => {
  const logger = serviceLocator.get("logger");
  const fileSystem = serviceLocator.get("fs");
  const axios = serviceLocator.get("axios");
  const cloudinary = serviceLocator.get("cloudinary");

  return new ThumbnailService(logger, fileSystem, axios, cloudinary);
});

serviceLocator.register("thumbnailController", () => {
  const Thumbnail = serviceLocator.get("thumbnailService");
  const logger = serviceLocator.get("logger");

  return new ThumbnailController(Thumbnail, logger);
});

module.exports = serviceLocator;
