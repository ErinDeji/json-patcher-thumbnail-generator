'use strict'
const errors = require('../utils/error')
const httpStatus = require('http-status')

class PatchController {
  constructor(patchService, logger) {
    this.patchService = patchService
    this.logger = logger
  }

  jsonPatch = (req, res) => {
    return this.patchService
      .initiateJsonPatch(req.body)
      .then((resp) => {
        this.logger.log('Response patch data: ', resp)
        return res.status(httpStatus.OK).send({ data: resp })
      })
      .catch((err) => {
        this.logger.error(err)
        this.logger.error('error from Patch Controller: ', err)
        return res
          .status(httpStatus.BAD_REQUEST)
          .send(new errors.BadRequestError(err.message.toString()))
      })
  }
}

module.exports = PatchController
