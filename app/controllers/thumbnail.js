'use strict'
const errors = require('../utils/error')
const httpStatus = require('http-status')

class ThumbnailController {
  constructor(thumbnailService, logger) {
    this.thumbnailService = thumbnailService
    this.logger = logger
  }

  thumbnailGenerator = (req, res) => {
    return this.thumbnailService
      .generateThumbnail(req.body)
      .then((resp) => {
        this.logger.log('Response thumbnail data: ', resp)
        return res.status(httpStatus.OK).send({ data: resp })
      })
      .catch((err) => {
        this.logger.error(err)
        this.logger.error('error from Thumbnail Controller: ', err)
        return res
          .status(httpStatus.BAD_REQUEST)
          .send(new errors.BadRequestError(err.message.toString()))
      })
  }
}

module.exports = ThumbnailController
