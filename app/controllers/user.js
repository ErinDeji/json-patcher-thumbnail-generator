'use strict'
const errors = require('../utils/error')
const httpStatus = require('http-status')

class UserController {
  constructor(userService, logger) {
    this.userService = userService
    this.logger = logger
  }

  createUser = (req, res) => {
    // this.logger.error(req, 'request is')

    return this.userService
      .createUser(req.body)
      .then((resp) => {
        this.logger.log('Response createUser: ', resp)
        return res.status(httpStatus.OK).send({ data: resp })
      })
      .catch((err) => {
        this.logger.log(err)
        this.logger.log('error from User Controller: ', err)
        if (err && err.code === 11000) {
          return res
            .status(httpStatus.FORBIDDEN)
            .send(new errors.ItemAlreadyExist(err.message.toString()))
        }
        this.logger.log(err.message)
        return res
          .status(httpStatus.BAD_REQUEST)
          .send(new errors.BadRequestError(err.message.toString()))
      })
  }

  loginUser = (req, res) => {
    return this.userService
      .loginUser(req.body)
      .then((resp) => {
        this.logger.log('Response loginUser: ', resp)
        return res.status(httpStatus.OK).send({ data: resp })
      })
      .catch((err) => {
        this.logger.log('error from User Controller: ', err)
        this.logger.log(err.message)
        return res
          .status(httpStatus.BAD_REQUEST)
          .send(new errors.BadRequestError(err.message.toString()))
      })
  }
}

module.exports = UserController
