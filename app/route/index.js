const userRoute = require("./user");
const patchRoute = require("./json-patch");
const thumbnailRoute = require("./thumbnail");

module.exports = {userRoute, patchRoute, thumbnailRoute}