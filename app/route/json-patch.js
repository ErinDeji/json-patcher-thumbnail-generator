const express = require('express');
const router = express.Router();
const di = require('../config/di');
const patchController = di.get('patchController');

router.post('/patch/create', patchController.jsonPatch)

module.exports = router;
