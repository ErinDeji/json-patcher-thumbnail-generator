const express = require("express");
const router = express.Router();
const di = require("../config/di");
const thumbnailController = di.get("thumbnailController");

router.post("/thumbnail/create", thumbnailController.thumbnailGenerator);

module.exports = router;
