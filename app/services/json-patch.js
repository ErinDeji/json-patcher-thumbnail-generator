/** @format */
"use strict";

class PatchService {
  constructor(logger, jsonPatch) {
    this.logger = logger;
    this.jsonPatch = jsonPatch;
  }

  initiateJsonPatch = (payload) => {
    return new Promise((resolve, reject) => {
      const obj = payload.obj;
      const patch = payload.patch;
      try {
        const data = this.jsonPatch.apply(obj, patch);
        this.logger.log(data);
        return resolve(data);
      } catch (error) {
        return reject(error);
      }
    });
  };
}

module.exports = PatchService;
