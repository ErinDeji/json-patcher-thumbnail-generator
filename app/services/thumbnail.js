/** @format */
"use strict";

class ThumbnailService {
  constructor(logger, fs, axios, cloudinary) {
    this.axios = axios;
    this.logger = logger;
    this.fs = fs;
    this.cloudinary = cloudinary
  }

  generateThumbnail = (payload) => {
    this.logger.log(payload);
    return new Promise((resolve, reject) => {
      let dir = "./tmp";
      const download_image = async (url, image_path) =>
        this.axios({
          url,
          responseType: "stream",
        }).then(
          (response) =>
            new Promise((resolve, reject) => {
              response.data
                .pipe(this.fs.createWriteStream(image_path))
                .on("finish", () => resolve(true))
                .on("error", (e) => reject(e));
            })
        );

      if (!this.fs.existsSync(dir)) {
        this.fs.mkdirSync(dir);
      }
      let image = `${dir}/${payload?.file_name}.png`;

      download_image(payload?.image_url, image)
        .then(async (res, err) => {
          if (res) {
            const result = await this.cloudinary.upload(`./tmp/${payload?.file_name}.png`);
            const data = {
              resized_image: result.url,
            };
            return resolve(data);
          }
          return reject(err);
        })
        .catch((err) => this.logger.error(err));
    });
  };
}

module.exports = ThumbnailService;
