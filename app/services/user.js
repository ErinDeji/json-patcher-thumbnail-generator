/** @format */
"use strict";

const config = require("../config/config");
const User = require("../model/user");
const jwt = require("jsonwebtoken");
const Errors = require("../utils/error");

class UserService {
  constructor() {
    this.logger = require("turbo-logger").createStream({});
  }

  createUser = (payload) => {
    return new Promise((resolve, reject) => {
      const { username, password } = payload;
      return new User({ username, password })
        .save()
        .then((res) => {
          this.logger.log("User Saved");
          // generate a token and send
          const token = jwt.sign(
            {
              password: res.password,
              username: res.username,
            },
            config.JWT_SECRET,
            {
              expiresIn: "7d",
            }
          );
          this.logger.log("Token generated");
          return resolve(token);
        })
        .catch((err) => {
          this.logger.error("error occured", err);
          return reject(err);
        });
    });
  };

  loginUser = (payload) => {
    return new Promise((resolve, reject) => {
      const { username, password } = payload;
      User.findOne({ username }).exec((err, user) => {
        if (err || !user) {
          return reject(new Errors.BadRequestError("User with that email does not exist. Please signup"));
        }

        // authenticate
        if (!user.authenticate(password)) {
          return reject(new Errors.BadRequestError("Email and password do not match"));
        }

        // generate a token and send to client
        const token = jwt.sign({ _id: user._id, username: user.username, password: user.password }, config.JWT_SECRET, {
          expiresIn: "7d",
        });
        const { _id, username } = user;

        const data = {
          token,
          user: { _id, username },
        };
        return resolve(data);
      });
    });
  };
}

module.exports = UserService;
