'use strict';

const cloudinary = require('cloudinary').v2;
const config = require('../config/config');
cloudinary.config(config.cloudinary);

class Uploader {
    constructor(logger) {
        this.logger = logger;
    }
    /**
     * Upload file buffer to Cloudinary
     * @param {Buffer} file 
     */
    async upload(file) {
        return new Promise(async (resolve) => {
            return cloudinary.uploader.upload(file, {height: 50, width: 50}, (err, res) => {
                if(err) {this.logger.error('cloudinary error', err)}
                resolve(res);
            })
        })
    }
}

module.exports = Uploader;