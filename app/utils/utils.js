module.exports = {
    /**
     * Parse Model object
     * @param {Value} value 
     */
    fileType: ({ file }) => {
        if (file.type && file.type.match(/video/ig)) {
            return 'video'
        }

        if (file.type && file.type.match(/image/ig)) {
            return 'image'
        }
        return 'image';
    },
}