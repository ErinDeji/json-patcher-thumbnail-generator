const { check } = require('express-validator');

module.exports = [
  check('username')
    .not()
    .isEmpty()
    .withMessage('username is required'),
  check('password')
    .isLength({ min: 3 })
    .withMessage('Password must be at least 3 characters long')
];